/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var INFO = {};
INFO.funcionar = function(){
    
};

INFO.insertar = function(){
    
    var xhr = new XMLHttpRequest();
    
    xhr.open("POST", "ejecutor");
    
    xhr.onreadystatechange = function(){
        
        if(xhr.onreadystatechange === 4 && xhr.status === 200){
            
            document.querySelector(comando.elpanel).innerHTML ="OK";
                        
        }
               
    };
    
    var actual={};
    //actual.id = document.querySelector("#").value;  -- No hace falta traer un ID para un insert
    actual.nombre = document.querySelector("#elnombre").value;
    actual.apellido = document.querySelector("#elapellido").value;
    actual.dni = document.querySelector("#eldni").value;
    //alert(JSON.stringify(actual)); 
    
    
    xhr.send(JSON.stringify(actual)); //Acá convierte a string lo que está chupando del query
    
};

INFO.consultar = function(comando){
    
    var xhr = new XMLHttpRequest();
    
    xhr.open("GET", comando.laurl);
    
    xhr.onreadystatechange = function(){
        
        if(xhr.readyState === 4 && xhr.status === 200)
        {
          
          var lavista = document.querySelector(comando.laplan).innerHTML;
          var losdatos = {};
          
          losdatos.listainfo = JSON.parse(xhr.responseText);
         // alert(JSON.stringify(losdatos));
          //alert(JSON.stringify(lavista));
          var resultado = Mustache.render(lavista, losdatos);
          
          document.querySelector(comando.elpanel).innerHTML = resultado;
            
            
        }
    };
    
    xhr.send();

    };
    

INFO.modificar = function( comando ){
    
    
    var xhr = new XMLHttpRequest();
    
    xhr.open("PUT", "ejecutor");
    
    xhr.onreadystatechange = function(){
        
        if(xhr.onreadystatechange === 4 && xhr.status === 200){
            
            document.querySelector(comando.elpanel).innerHTML ="OK";
                        
        }
               
    };
    
    var actual={};
    actual.id = document.querySelector(comando.linea_id).value;
    actual.apellido = document.querySelector(comando.linea_ape).value;
    actual.nombre = document.querySelector(comando.linea_nom).value;
    actual.dni = document.querySelector(comando.linea_dni).value;    
    
    
    xhr.send(JSON.stringify(actual)); //Acá convierte a string lo que está chupando del query
    
   
};

INFO.eliminar = function( comando ){

        var actual={};
    actual.id = document.querySelector(comando.linea_id).value;

    
    var xhr = new XMLHttpRequest();
    
    xhr.open("DELETE", "ejecutor?&q="+JSON.stringify(actual));
    
    xhr.onreadystatechange = function(){
        
        if(xhr.readyState === 4 && xhr.status === 200){
            
            document.querySelector(comando.elpanel).innerHTML += xhr.responseText + '<br/>';
                        
        }
       // xhr.send( );     
    };
    
    // actual.apellido = document.querySelector(comando.linea_ape).value;
    // actual.nombre = document.querySelector(comando.linea_nom).value;
    // actual.dni = document.querySelector(comando.linea_dni).value;    
    
    
    xhr.send(); //Acá convierte a string lo que está chupando del query
    
   
};


INFO.inicializar = function(){

    document.querySelector("#elboton").setAttribute("onclick","INFO.consultar({'laurl':'ejecutor','laplan':'#laplantilla','elpanel':'#panelResultado'})"); 
    document.querySelector("#elbotoninsertar").setAttribute("onclick","INFO.insertar({'laurl':'ejecutor','laplan':'#laplantilla','elpanel':'#panelResultado'});");
    
};
INFO.inicializar();
   