/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejecutor;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.*;
import com.google.gson.Gson;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;


/**ﬁ
 *
 * @author Actaeon
 */
@WebServlet(name = "ejecutor", urlPatterns = {"/ejecutor"})
public class ejecutor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ejecutor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ejecutor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        PrintWriter out = response.getWriter();
        
        
        try // GET : Aca se ejecuta la consulta a la BD, en este caso "SELECT"
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/inventario", "root", "");
            PreparedStatement ejecutar_query = conexion.prepareStatement("SELECT * FROM usuarios");
            ResultSet respuesta_query = ejecutar_query.executeQuery();
            
            //Array donde se guarda la consulta hecha
            ArrayList<TreeMap> listado = new ArrayList(); 
            
            while(respuesta_query.next())
            {
                TreeMap datosActuales = new TreeMap();
                datosActuales.put("usu_id", respuesta_query.getString("usu_id"));
                datosActuales.put("usu_nombre", respuesta_query.getString("usu_nombre"));
                datosActuales.put("usu_apellido", respuesta_query.getString("usu_apellido"));
                datosActuales.put("usu_dni", respuesta_query.getString("usu_dni"));
                
                listado.add(datosActuales);      
                
                
            }
            Gson convertir_texto = new Gson();
            out.print(convertir_texto.toJson(listado));
            
            
        }
        catch(Exception ex)
        {
            out.printf(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    
        PrintWriter out = response.getWriter();
    
        
        
        try
    {
        Gson convertir_texto = new Gson();
        TreeMap <String, String> parametros = convertir_texto.fromJson(request.getReader(), TreeMap.class);
       Class.forName("com.mysql.jdbc.Driver").newInstance();
       Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/inventario", "root", "");
       PreparedStatement ejecutar_query = conexion.prepareStatement("INSERT INTO usuarios(usu_nombre, usu_apellido, usu_dni) VALUES(?,?,?)");
       ejecutar_query.setString(1,parametros.get("nombre"));
       ejecutar_query.setString(2,parametros.get("apellido"));
       ejecutar_query.setString(3,parametros.get("dni"));
       
       ejecutar_query.execute();
    }
    
    catch(Exception ex)
    {
       out.printf(ex.getMessage());
       ex.printStackTrace(); 
    }
    }
//Rest
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doPut(req, resp); //To change body of generated methods, choose Tools | Templates.
        
        PrintWriter out = resp.getWriter();
    
        
        
        try
    {
       Gson convertir_texto = new Gson();
       TreeMap <String, String> parametros = convertir_texto.fromJson(req.getReader(), TreeMap.class);
       Class.forName("com.mysql.jdbc.Driver").newInstance();
       Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/inventario", "root", "");
       PreparedStatement ejecutar_query = conexion.prepareStatement("UPDATE usuarios set usu_nombre=?, usu_apellido=?, usu_dni=?  where usu_id =? ");
       ejecutar_query.setString(4,parametros.get("id"));
       ejecutar_query.setString(1,parametros.get("nombre"));
       ejecutar_query.setString(2,parametros.get("apellido"));
       ejecutar_query.setString(3,parametros.get("dni"));
System.out.println("CUALQUIERCOSA!!!!!!!!!!!!!!!!"+ ejecutar_query );       
       
       //Hacía mal el update porque estaba mal puesto el orden en que se designaban los parametros.
       //Por eso a veces podía modificar y a veces no, en una sola casilla en particular.
       
       
       ejecutar_query.execute();
       
    }
    
    catch(Exception ex)
    {
       out.printf(ex.getMessage());
       ex.printStackTrace(); 
    }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doDelete(req, resp); //To change body of generated methods, choose Tools | Templates.
    
        PrintWriter out = resp.getWriter();
        System.out.println("!!!   AAA   !!!");
        try
    {
       Gson convertir_texto = new Gson();
//       TreeMap <String, String> parametros = convertir_texto.fromJson(req.getReader(), TreeMap.class);
       TreeMap <String, String> parametros = convertir_texto.fromJson(req.getParameter("q"), TreeMap.class);
       Class.forName("com.mysql.jdbc.Driver").newInstance();
       Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/inventario", "root", "");
       PreparedStatement ejecutar_query = conexion.prepareStatement("DELETE from usuarios where usu_id =? ");
       ejecutar_query.setString(1,parametros.get("id"));
System.out.println("CUALQUIERCOSA!!!!!!!!!!!!!!!!"+ ejecutar_query );       

       ejecutar_query.execute();
       //JOptionPane.showMessageDialog(null,"borro"); Como se hace para ver el query de lo que esta borrando?
       
    }
    
    catch(Exception ex)
    {
       out.printf(ex.getMessage());
       ex.printStackTrace(); 
    }
    
    
    
    }
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    } 

